; It takes about 0.8 nanoseconds per element. For example for 10_000_000
; elements it took 8_804_129 ns. For 1_000_000 it took 757_197 ns.
;
; There's a lower limit of 1000 ns. Even for 2 elements it takes ~1000ns. For
; 1000 eleemnts it takes 1600ns.
;
; Note that all these times represent only the algorithm time. Most of the time
; is spent reading the numbers. The entire program takes 0.7 seconds for
; 10_000_000 elements.
global _start
extern printf
extern scanf
extern exit
extern malloc

%define CLOCK_MONOTONIC 1
%define SYS_CLOCK_GETTIME 228

section .data
  fmt_d: db "%d ", 10, 0
  scan_d: db "%d", 0
  scan_addr: dq 0

  struc timespec
    tv_sec: resq 1
    tv_nsec: resq 1
  endstruc

  time_struct: istruc timespec
    at tv_sec, dq 0
    at tv_nsec, dq 0
  iend

section .text

_start:
  ; r15 = target
  mov rdi, scan_d
  lea rsi, [rel scan_addr]
  call scanf
  mov r15, [rel scan_addr]

  ; r14 = length_of_nums
  mov rdi, scan_d
  lea rsi, [rel scan_addr]
  call scanf
  mov r14, [rel scan_addr]

  ; rbx = pointer to array `nums`
  mov rax, 8
  mov rbx, r14
  mul rbx
  mov rdi, rax
  call malloc
  mov rbx, rax

  ; r12 = how many numbers left to read
  mov r12, r14
  ; r13 = address to store read number, incremented by 8
  mov r13, rbx

read_loop:
  cmp r12, 0
  jz done_reading

  mov rdi, scan_d
  mov rsi, r13
  call scanf

  dec r12
  add r13, 8
  jmp read_loop

done_reading:

  ; Save the nano time at this point (the start).
  mov rax, SYS_CLOCK_GETTIME
  mov rdi, CLOCK_MONOTONIC
  mov rsi, time_struct
  syscall

  ; r10 is `nums`
  mov r10, rbx
  ; rax starts with the maximum index and goes to 1
  mov rax, r14
  ; r9 is target
  mov r9, r15

next_rax:
  dec rax
  cmp rax, 0
  je failed

  ; rbx starts with rax - 1 and goes to 0
  mov rbx, rax

next_rbx:
  dec rbx
  cmp rbx, -1
  je next_rax

  mov rcx, [r10 + 8 * rax]
  add rcx, [r10 + 8 * rbx]

  cmp rcx, r9
  je found
  jmp next_rax

found:
  ; save the indices to r15,r14
  mov r15, rax
  mov r14, rbx

  ; save the nanosecond
  mov r13, [time_struct + tv_nsec]

  ; Get the end nano second.
  mov rax, SYS_CLOCK_GETTIME
  mov rdi, CLOCK_MONOTONIC
  mov rsi, time_struct
  syscall

  ; Print the time diff.
  mov rdi, fmt_d
  mov rsi, [time_struct + tv_nsec]
  sub rsi, r13
  call printf

  ; Print first index.
  mov rdi, fmt_d
  mov rsi, r14
  call printf

  ; Print second index.
  mov rdi, fmt_d
  mov rsi, r15
  call printf

  jmp done

failed:
  mov rdi, 1
  call exit

done:
  mov rdi, 0
  call exit
